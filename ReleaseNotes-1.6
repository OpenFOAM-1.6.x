#                            -*- mode: org; -*-
#
#+TITLE:           OpenFOAM release notes for version 1.6
#+AUTHOR:                      OpenCFD Ltd.
#+DATE:                         July 2009
#+LINK:                  http://www.opencfd.co.uk
#+OPTIONS: author:nil ^:{}

* Overview
  OpenFOAM-1.6 is a significant upgrade to version 1.5 in ways that are
  outlined below.  This release passes all our standard tests and the
  tutorials have been broadly checked.  If there are any bugs, please report
  them using the instructions set out here:
  http://www.OpenFOAM.org/bugs.html.

* GNU/Linux version
  The 32bit and 64bit binary packs of the OpenFOAM release were compiled on
  a machine running openSUSE GNU/Linux version 11.1 and also tested on
  Ubuntu 9.  We recommend that users run OpenFOAM on one of these, or on a
  similarly recent version of GNU/Linux.  This release has also been
  successfully compiled and tested on older GNU/Linux releases, but this
  requires the installation of Qt 4.3.? (the sources for which are supplied
  with OpenFOAM-1.6, see README) for ParaView-3 to run.

* C++ Compiler version
  + Release compiled with GCC 4.3.3.
  + Built-in support for the Intel C++ 10.? compiler (untested).
  + The choice of the compiler is controlled by the setting of the
    ~$WM_COMPILER~ and ~$WM_COMPILER_ARCH~ environment variables in the
    /OpenFOAM-1.6/etc/bashrc/ (or /cshrc/) file.
  + The location of the compiler installation is controlled by the
    ~$compilerInstall~ environment variable in the
    /OpenFOAM-1.6/etc/settings.sh/ (or /settings.csh/) file.

* Library developments

*** Core library

***** Dictionary improvements/changes
      + Dictionaries can use words (unquoted) or regular expressions (quoted)
        for their keywords. When searching, an exact match has priority over a
        regular expression match.  Multiple regular expressions are matched in
        reverse order.
      + The *new* =#includeIfPresent= directive is similar to the =#include=
        directive, but does not generate an error if the file does not exist.
      + The default =#inputMode= is now '=merge=', which corresponds to the most
        general usage. The =#inputMode warn= corresponds to the previous default
        behaviour.
      + The *new* =#inputMode protect= can be used to conditionally merge
        default values into existing dictionaries.
      + *New* =digest()= method to calculate and return the SHA1 message digest.

***** Regular Expressions
      The addition of regular expressions marks a major improvement in
      usability.
      + *New* =regExp= class provides support for accessing POSIX extended
        regular expresssions from within OpenFOAM.
      + *New* =wordRe= class can contain a =word= or a =regExp= .
      + *New* =stringListOps= to search string lists based on regular
        expressions, =wordRe= or =wordReList=.
      + =Istream= and =Ostream= now retain backslashes when reading/writing
        strings.

***** Convenience changes
      + =IOobject= has a *new* constructor for creating an =IOobject= from a
        single-path specification (eg, see =blockMesh -dict= option).
      + =argList= has *new* convenience methods for accessing options more
        directly: =option()=, =optionFound()=, =optionLookup()=, =optionRead()=,
        =optionReadIfPresent()=.
      + The *new* =readList(Istream&)= can read a bracket-delimited list or
        handle a single value as a list of size 1. This can be a useful
        convenience when processing command-line options.
      + Export *new* environment variable =FOAM_CASENAME= that contains the
        name part of the =FOAM_CASE= environment variable.

*** Turbulence modelling
    + Major development of turbulence model libraries to give extra flexibility
      at the solver level.  For solvers that can support either RAS/LES
      computations, the selection is made in the
      /constant/turbulenceProperties/, by setting the =simulationType= keyword
      to:
      - =laminar=,
      - =RASModel=,
      - =LESModel=.
    + Depending on the selection, the model is the instantiated from /constant//
      - /RASProperties/,
      - /LESProperties/.

***** RAS wall functions
      Wall functions are now run-time selectable per patch for RAS.
      + Velocity:
        - Apply to turbulent viscosities =nut= or =mut=,
        - Apply to =k=, =Q=, =R=,
        - Apply to =epsilon=, =omega=.
      + Temperature:
        - Apply to turbulent thermal diffusivity, =alphat= (compressible only).
      + To apply wall functions:
        - To recapture the functionality of previous OpenFOAM versions (v1.5 and
          earlier) assign:
          - for velocity:
            - =nut=: =nutWallFunction=,
            - =mut=: =muWallFunction=,
            - =epsilon=: =epsilonWallFunction=,
            - =omega=: =omegaWallFunction=,
            - =k=, =q=, =R=: =kqRWallFunction=.
          - for temperature:
            - =alphat=: =alphatWallFunction=.
        - New =alphaSgsJayatillekeWallFunction= thermal wall function for
          compressible LES.

***** *New* LES turbulence models
      + Spalart-Allmaras DDES.
      + Spalart-Allmaras IDDES.

***** Upgrading:
      + *New* utility - =applyWallFunctionBoundaryConditions=.
      + Solvers will automatically update existing cases.
        - New fields created based on the presence of the =nut/mut= field.
        - Boundary conditions include scoping, i.e compressibility:: for
          compressible solvers.
        - Modified fields will be backed-up to /<field>.old/.
      + NOTE:
        - Fields are only updated for those fields associated with the current
          turbulence model selection, i.e. if fields exist for use with other
          models, they will not be updated.
        - The new specification is not backwards compatible.

*** Thermo-physical Models
    + Old compressibility-based thermo package renamed
      =basicThermo= \rightarrow =basicPsiThermo=.
    + *New* =basicRhoThermo= thermo package.
      - Additional density field stored.
      - General form - can be used for other types of media, e.g. liquids.
      - Additional polynomial-based thermodynamics:
        - Equation of state: =icoPolynomial=,
        - Transport: =polynomialTransport=,
        - Thermo: =hPolynomialThermo=.
    + Removed earlier hard-coding of gas thermophysics for chemistry modelling:
      - =reactingMixture= now templated on thermo package,
      - =chemistryModel= now templated on thermo package,
      - =chemistrySolver= now templated on thermo package.
    + *New* =fvDOM= radition model
      - finite volume, discrete ordinates method.
    + *New* (reinstated) =eThermo= thermodynamics package
      - internal energy-based thermodynamics.

*** Lagrangian

***** Intermediate
      + Overhaul of the underlying framework.
      + Reacting now split into reacting and reacting multiphase.
      + New structure for variable composition.
      + Many new sub-models, including:
        - Injection
          - =PatchInjection= - injection local to patch face cells,
          - =FieldActivatedInjection= - injection based on satisfying external
            criterion,
          - LookupTableInjection - explicity define injection locations and all
            parcel properties.
        - Post-processing
          - patch post-processing - collect data for parcels impacting user,
            defined patches.
        - Patch interaction
          - generalised behaviour for parcel interaction with patch.
        - Phase change
          - liquid evaporation.

***** Coal combustion
      + *New* library - extension of reacting-multiphase functionality.
        - Surface reaction/combustion models.

*** Discrete methods
    + *New* library offering DSMC simulation functionality - see =dsmcFoam=
      below.
    + Significant development of the libraries offering molecular dynamics
      simulation functionality - see =mdFoam= and =mdEquilibrationFoam= below.

*** Numerics
    + *new* polynomial-fit higher-order interpolation schemes:
      - =biLinearFit=
      - =linearFit=
      - =quadraticLinearFit=
      - =quadraticFit=
      - =linearPureUpwindFit=
      - =quadraticLinearPureUpwindFit=
      - =quadraticLinearUpwindFit=
      - =quadraticUpwindFit=
      - =cubicUpwindFit=
    + *new* polynomial-fit higher-order Sn-Grad: =quadraticFitSnGrad=.

*** *New* surfMesh library
    Provides a more efficient storage mechanism than possible with =triSurface=
    without restrictions on the shape of the face (templated parameter).
    + =MeshedSurface= class - with zero or more contiguous =surfZones= .
    + =UnsortedMeshedSurface= class - unordered surface zones (as per
      =triSurface=).
    + =surfMesh= class - for reading/writing in native OpenFOAM format.

* Solvers
*** Solver restructuring
    The upgrade to the turbulence models means that the simulation type, i.e.
    laminar, RAS or LES can be selected at run time. This has allowed a reduction
    in the number of solvers, simplifying the overall code structure
    + Solvers which support laminar, RAS and LES:
      - =turbFoam=, =oodles= \rightarrow =pisoFoam=.
      - =turbDyMFoam= \rightarrow =pimpleDyMFoam=.
      - =rhoTurbFoam=, =coodles= \rightarrow =rhoPisoFoam=.
      - =xoodles= \rightarrow absorbed into =XiFoam=.
      - =buoyantFoam=, =lesBuoyantFoam= \rightarrow =buoyantPisoFoam=.
      - =interFoam=, =rasInterFoam=, =lesInterFoam= \rightarrow =interFoam=.
      - =lesCavitatingFoam=, =rasCavitatingFoam= \rightarrow =cavitatingFoam=.
    + Solvers which support LES only:
      - =channelOodles= \rightarrow =channelFoam= (LES).
    + =pd= replaced by static pressure =p=.  All solvers in which buoyancy affects
      might be strong have been converted from using =pd= to =p= with improved
      numerics to give equally good accuracy and stability.  This change is
      prompted by the need to remove the confusion surrounding the meaning and
      purpose of =pd=.
    + =g= (acceleration due to gravity) is now a *new*
      =uniformDimensionedVectorField= which has the behaviour of a field, is
      registered to an =objectRegistry=, but stores only a single value.  Thus
      =g= and other =UniformDimensionedFields= can be created and looked-up
      elsewhere in the code, /e.g./ in =fvPatchFields=.

*** Solver control improvements
    Now uses consistent dictionary entries for the solver controls.
    + This Allows dictionary substitutions and regular expressions in
      /system/fvSolution/.
    + The old solver control syntax is still supported (warning emitted), but
      the *new* =foamUpgradeFvSolution= utility can be used to convert
      /system/fvSolution/ to the new format.

*** *New* Solvers
    + =buoyantBoussinesqSimpleFoam= Steady state heat transfer solver using a
      Boussinesq approximation for buoyancy, with laminar, RAS or LES turbulence
      modelling.
    + =buoyantBoussinesqPisoFoam= Transient heat transfer solver using a
      Boussinesq approximation for buoyancy, with laminar, RAS or LES turbulence
      modelling.
    + =coalChemistryFoam= Transient, reacting lagrangian solver, employing a coal
      cloud and a thermo cloud, with chemistry, and laminar, RAS or LES turbulence
      modelling.
    + =porousExplicitSourceReactingParcelFoam= Transient, reacting lagrangian
      solver, employing a single phase reacting cloud, with porous media, explicit
      mass sources, and laminar, RAS or LES turbulence modelling.
    + =rhoReactingFoam= Density-based thermodynamics variant of the reactingFoam
      solver, i.e. now applicable to liquid systems.
    + =dsmcFoam= DSMC (Direct Simulation Monte-Carlo) solver for rarefied gas
      dynamics simulations, able to simulate mixtures of an arbitrary number of
      gas species.  The variable hard sphere collision model with Larsen-Borgnakke
      internal energy redistribution (see "Molecular Gas Dynamics and the Direct
      Simulation of Gas Flows" G.A. Bird, 1994) is available; other run-time
      selectable collision models can be easily added.

*** Updated solvers
    + =mdFoam= Molecular Dynamics (MD) solver able to simulate a mixture of an
      arbitrary number of mono-atomic and small, rigid polyatomic (i.e. H2O, N2)
      molecular species, with 6 degree of freedom motion, in complex geometries. A
      molecule of any species can be built by specifying its sites of mass and
      charge. All molecules interact with short-range dispersion forces and
      pairwise electrostatic interactions using methods described in: Fennell and
      Gezelter, J. Chem. Phys. 124, 234104 (2006).
    + =mdEquilibrationFoam= Similar to mdFoam, but employs velocity scaling to
      adjust the simulation temperature to a target value. Useful to equilibrate a
      case before simulation.
    + =chtMultiRegionFoam= New boundary condition allows independent decomposition
      of coupled regions without any constraint on the decomposition.

* Boundary conditions
  + Improved set of direct mapped boundary conditions.
  + =buoyantPressureFvPatchScalarField=, the *new* buoyancy pressure boundary
    condition now supports =p= and =pd= for backward compatibility.
  + =uniformDensityHydrostaticPressure= is an additional pressure boundary
    condition to aid the transition from =pd= to =p= as it behaves similarly to
    specifying a uniform =pd= at an outlet for example.
  + =activeBaffleVelocity= dynamically combines cyclic and wall patches so that
    the flow through the patch can be controlled /e.g./ by pressure drop.
  + =rotatingWallVelocity= specifies a rotating velocity, given the rotational
    speed, origin and axis.

* Utilities

*** Improvements
   + =blockMesh= has a *new* =-dict= option for specifying an alternative
     dictionary for the block mesh description. The '=convertToMeters=' entry
     is now optional, and the alternative '=scale=' entry can be used for
     less typing.
   + =foamToEnsight= has a *new* =-noPatches= option to suppress generation
     of patches.
   + =foamToEnsightParts= has *new* =-noMesh= and =-index= options that can
     be useful when post-processing results incrementally.
   + =snappyHexMesh= has lower memory footprint. New distributed triangulated
     surface type for meshing surfaces with extremely large triangle count.
     Now supports multi-region meshing of arbitrarily complex regions.

*** *New* utilities
   + =particleTracks= - generate particle tracks for lagrangian calculations.
   + =dsmcInitialise= - preprocessing utility to create initial configurations
     of DSMC particles in a geometry.
   + =surfaceRedistributePar= - preprocessing utility to create distributed
     triangulated surface.

*** *New* foamCalc functions
    + =interpolate= performs fvc::interpolate(<field>).
    + =randomise= randomises a <field> by a given perturbation.
    + =addSubtract= provides simple add/subtract field functionality.

*** Usage
   + =timeSelector= can now combine =-time ranges= and =-latestTime= options.
     For example, -time '0.01:0.09' -latestTime vs. -time '0.01:'.
     More reliable behaviour for cases missing /constant// or /0// directories.
     When the =-noZero= option is enabled, =-latestTime= will not select the
     =0/= directory unless the =-zeroTime= option is given.
     This helps avoid ill effects caused by accidentally using the
     /0// directory in certain utilities (eg, =reconstructPar=).
   + =-region= option added to more utilities.

*** Improvements to Paraview reader module
   + =PV3FoamReader= added mesh region handling. The region name is parsed
     from the filename. Eg, /case{region}.OpenFOAM/.
   + =paraFoam= with a *new* =-region= option for specifying an alternative
     region. A *new* =-touch= option to generate the /.OpenFOAM/ file only.
     Only creates (and removes) /.OpenFOAM/ files if they didn't already
     exist, which is useful in connection with the =-touch= option.

* Post-processing
  + Sampling on iso-surfaces, interpolated or non-interpolated.
  + Sampling on surface defined by distance to surface (=distanceSurface=).
  + Cutting planes for arbitrary meshes.
  + Output to any surface geometry format supported by the =surfMesh= library.

*** Function objects

***** Improvements for function objects and time-looping
      + The =functionObjectList= retains the order of the =functionObject=
        order, which allows a chaining of operations. It is thus internally more
        efficient when /system/controlDict/ uses =functions {..}= instead of
        =functions (..)=, but both forms are supported.
      + The =functionObject= now has an additional =end()= method that is called
        when =Time::loop()= or =Time::run()= determine that the time-loop exits.
        Accordingly, one of these two idioms should be used in solver code:
        1. =while (runTime.loop() { ... }=,
        2. =while (runTime.run()) { runTime++; ... }=.
      + *New* =functionObjectList= now tracks the SHA1 message digest of the
        sub-directories. This avoids reloading a =functionObject= when
        something unrelated in /system/controlDict/ changed.

***** *New* function objects:
       + =systemCall= - executes a list of system instructions.
       + =fieldMinMax= - computes the min/max of a <field>.
       + =staticPressure= - converts kinematic pressure to static pressure.
       + =dsmcFields= - calculates intensive fields (velocity and temperature)
         from averaged extensive fields (i.e. momentum and energy).

***** Usage
       + Improved output control: =timeStep= or =outputTime=.

* Tutorial restructuring
  to reflect solver application structure.

* Third-party Software
  + =gcc= upgraded to version 4.3.3.
  + =OpenMPI= upgraded to version 1.3.3.
  + =ParaView= upgraded to version 3.6.1.
  + =Scotch= *new* decomposition method: \\
    Scotch (http://gforge.inria.fr/projects/scotch/) is a general multi-level
    decomposition method originating from the ScAlApplix project (Inria). It is
    a framework for general recursive partitioning methods and a such comparable
    to Metis but with a permissive licence.

    The corresponding decomposition method (in =decomposeParDict=) is
    =scotch=.  An optional =strategy= string can be supplied to change the
    decomposition methods; initial testing shows the default strategy producing
    decompositions comparable in quality to Metis.
