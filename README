#                            -*- mode: org; -*-
#
#+TITLE:             OpenFOAM README for version 1.6
#+AUTHOR:                      OpenCFD Ltd.
#+DATE:                         July 2009
#+LINK:                  http://www.opencfd.co.uk
#+OPTIONS: author:nil ^:{}

* Copyright
  OpenFOAM is free software; you can redistribute it and/or modify it under the
  terms of the GNU General Public License as published by the Free Software
  Foundation; either version 2 of the License, or (at your option) any later
  version.  See the file COPYING in this directory, for a description of the GNU
  General Public License terms under which you can copy the files.

* System requirements
  OpenFOAM is developed and tested on Linux, but should work with other POSIX
  systems.  To check your system setup, execute the foamSystemCheck script in
  the bin/ directory of the OpenFOAM installation. If no problems are reported,
  proceed to "3. Installation"; otherwise contact your system administrator.

  If the user wishes to run OpenFOAM in 32/64-bit mode they should consult the
  section "Running OpenFOAM in 32-bit mode".

*** Qt (from http://trolltech.com/products/qt)
    The ParaView 3.6.1 visualisation package requires Qt to be installed on the
    system.  ParaView's producers state that ParaView is only officially
    supported on Qt version 4.3.x.  However, we have found in limited tests that
    ParaView works satisfactorily with newer versions of Qt than 4.3.x.  To
    check whether Qt4 is installed, and the version, type:
    + qmake --version

    Both 32- and 64-bit version of ParaView were compiled with Qt-4.4.3 (with
    openSuSE-11.1).  If the user finds that a ParaView binary fails to run, then
    it is almost certainly due to a conflict in compiled and installed Qt
    versions and they will need to consult the section below on "Compiling
    ParaView and the PV3FoamReader module."

    The default versions of Qt used by some GNU/Linux releases are as follows.
    + ubuntu-7.10:   Version 4.3.2
    + ubuntu-8.04:   Version 4.3.4
    + ubuntu-9.04:   Version 4.5.0
    + openSuSE-10.2: Version 4.2.1 - too old
    + openSuSE-10.3: Version 4.3.1
    + openSuSE-11.0: Version 4.4.0
    + openSuSE-11.1: Version 4.4.3

    Compilation and running of ParaView has been successful using the libraries
    downloaded in the "libqt4-dev" package on ubuntu.

    If you don't have an appropriate version of Qt installed you can download
    the sources from TrollTech e.g.:
    ftp://ftp.trolltech.com/qt/source/qt-x11-opensource-src-4.3.5.tar.bz2
    and compile and install in /usr/local or some other location that does to
    conflict with the pre-installed version.

* Installation
  Download and unpack the files in the $HOME/OpenFOAM directory as described in:
  http://www.OpenFOAM.org/download.html

  The environment variable settings are contained in files in an etc/ directory
  in the OpenFOAM release. e.g. in

  + $HOME/OpenFOAM/OpenFOAM-1.6/etc/

  1) EITHER, if running bash or ksh (if in doubt type 'echo $SHELL'), source the
    etc/bashrc file by adding the following line to the end of your
    $HOME/.bashrc file:

    + . $HOME/OpenFOAM/OpenFOAM-1.6/etc/bashrc

    Then update the environment variables by sourcing the $HOME/.bashrc file by
    typing in the terminal:

    + . $HOME/.bashrc

  2) OR, if running tcsh or csh, source the etc/cshrc file by adding the
    following line to the end of your $HOME/.cshrc file:

    + source $HOME/OpenFOAM/OpenFOAM-1.6/etc/cshrc

    Then update the environment variables by sourcing the $HOME/.cshrc file by
    typing in the terminal:

    + source $HOME/.cshrc

*** Installation in alternative locations
    OpenFOAM may also be installed in alternative locations. However, the
    installation directory should be network available (e.g., NFS) if parallel
    calculations are planned.

    The environment variable 'FOAM_INST_DIR' can be used to find and source the
    appropriate resource file. Here is a bash/ksh/sh example:

    + export FOAM_INST_DIR=/data/app/OpenFOAM
    + foamDotFile=$FOAM_INST_DIR/OpenFOAM-1.6/etc/bashrc
    + [ -f $foamDotFile ] && . $foamDotFile

    and a csh/tcsh example:

    + setenv FOAM_INST_DIR /data/app/OpenFOAM
    + foamDotFile=$FOAM_INST_DIR/OpenFOAM-1.6/etc/cshrc
    + if ( -f $foamDotFile ) source $foamDotFile

    The value set in '$FOAM_INST_DIR' will be used to locate the remaining parts
    of the OpenFOAM installation.

* Building from Sources (Optional)
  If you cannot find an appropriate binary pack for your platform, you can build
  the complete OpenFOAM from the source-pack.  You will first need to compile or
  obtain a recent version of gcc (we recomend gcc-4.3.?) for your platform,
  which may be obtained from http://gcc.gnu.org/.

  Install the compiler in
  $WM_PROJECT_INST_DIR/ThirdParty/gcc-<GCC_VERSION>/platforms/$WM_ARCH$WM_COMPILER_ARCH/
  and change the gcc version number in $WM_PROJECT_DIR/etc/settings.sh and
  $WM_PROJECT_DIR/etc/settings.csh appropriately and finally update the
  environment variables as in section 3.

  Now go to the top-level source directory $WM_PROJECT_DIR and execute the
  top-level build script './Allwmake'.  In principle this will build everything,
  but if problems occur with the build order it may be necessary to update the
  environment variables and re-execute 'Allwmake'.

  If you experience difficulties with building the source-pack, or your platform
  is not currently supported, please contact <enquiries@OpenCFD.co.uk> to
  negotiate a support contract and we will do the port and maintain it for
  future releases.

* Testing the installation
  To check your installation setup, execute the 'foamInstallationTest' script
  (in the bin/ directory of the OpenFOAM installation). If no problems are
  reported, proceed to getting started with OpenFOAM; otherwise, go back and
  check you have installed the software correctly and/or contact your system
  administrator.

* Getting Started
  Create a project directory within the $HOME/OpenFOAM directory named
  <USER>-1.6 (e.g. 'chris-1.6' for user chris and OpenFOAM version 1.6)
  and create a directory named 'run' within it, e.g. by typing:

  + mkdir -p $FOAM_RUN/run

  Copy the 'tutorial' examples directory in the OpenFOAM distribution to the
  'run' directory.  If the OpenFOAM environment variables are set correctly,
  then the following command will be correct:

  + cp -r $WM_PROJECT_DIR/tutorials $FOAM_RUN

  Run the first example case of incompressible laminar flow in a cavity:

  + cd $FOAM_RUN/tutorials/incompressible/icoFoam/cavity
  + blockMesh
  + icoFoam
  + paraFoam

  Refer to the OpenFOAM User Guide at http://www.OpenFOAM.org/doc/user.html for
  more information.

* Compiling Paraview 3.6.1 and the PV3FoamReader module
  If there are problems encountered with ParaView, then it may be necessary to
  compile ParaView from sources.  The compilation
  is a fairly simple process using the supplied makeParaView script that
  has worked in our tests with other packages supplied in the ThirdParty
  directory, namely cmake-2.6.4 and gcc-4.3.3.  Execute the following:
  + cd $WM_THIRD_PARTY_DIR
  + rm -rf paraview-3.6.1/platforms
  + ./makeParaView

  The PV3FoamReader module is an OpenFOAM utility that can be compiled in the
  usual manner as follows:
  + cd $FOAM_UTILITIES/postProcessing/graphics/PV3FoamReader
  + ./Allwclean
  + ./Allwmake

*** Compiling Paraview with a local version of Qt
    If the user still encounters problems with ParaView, it may relate to the
    version of Qt, in which case, it is recommended that the user first
    downloads a supported version of Qt /e.g./ 4.3.5 as described in the section
    on "Qt".  The user should unpack the source pack in the $WM_THIRD_PARTY_DIR.
    Then the user can build Qt by executing from within $WM_THIRD_PARTY_DIR:
    + ./makeQt

    The user should then compile ParaView using the local version of Qt by
    executing makeParaView with the -qmake option, giving the full path of the
    newly built qmake as an argument:
    + ./makeParaView -qmake <path_to_qmake>

    The user must then recompile the PV3FoamReader module as normal (see above).

* Documentation
  http://www.OpenFOAM.org/doc

* Help
  http://www.OpenFOAM.org http://www.OpenFOAM.org/discussion.html

* Reporting Bugs in OpenFOAM
  http://www.OpenFOAM.org/bugs.html

* Running OpenFOAM in 32-bit mode on 64-bit machines
  Linux users with a 64-bit machine may install either the OpenFOAM 32-bit
  version (linux) or the OpenFOAM 64-bit version (linux64), or both.  The 64-bit
  is the default mode on a 64-bit machine.  To use an installed 32-bit version,
  the user must set the environment variable WM_ARCH_OPTION to 32 before
  sourcing the etc/bashrc (or etc/cshrc) file.
